package com.devcamp.bookauthorapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bookauthorapi.model.Author;
import com.devcamp.bookauthorapi.services.AuthorService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class AuthorController {
    @Autowired
    private AuthorService authorService;

    @GetMapping("/author-info")
    public Author getAuthorInfoByEmail(@RequestParam(name = "email", required = true) String authorEmail) {
        Author findAuthor = authorService.filterAuthorsByEmail(authorEmail);

        return findAuthor;
    }

    @GetMapping("/author-gender")
    public ArrayList<Author> getAuthorByGender(@RequestParam(name = "gender", required = true) char authorGender) {
        ArrayList<Author> findAuthor = authorService.filterAuthorsByGender(authorGender);

        return findAuthor;
    }
}
