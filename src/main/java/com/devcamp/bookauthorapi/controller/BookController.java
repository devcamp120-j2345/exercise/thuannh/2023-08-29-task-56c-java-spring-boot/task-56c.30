package com.devcamp.bookauthorapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bookauthorapi.model.Book;
import com.devcamp.bookauthorapi.services.BookService;
@RequestMapping("/")
@RestController
@CrossOrigin
public class BookController {
    @Autowired
    private BookService bookService;

    @GetMapping("/books")
    public ArrayList<Book> getAllBooks(){
        ArrayList<Book> allBook = bookService.getAllBooks();

        return allBook;
    }

    @GetMapping("/book-quantity")
    public ArrayList<Book> getBookQuantity(@RequestParam(name="qty", required = true) int bookQTy){
        ArrayList<Book> allBook = bookService.getAllBooks();

        ArrayList<Book> findBook = new ArrayList<>();

        for (Book bookElement : allBook) {
            if(bookElement.getQty() >= bookQTy){
                findBook.add(bookElement);
            }
        }
        
        return new ArrayList<>(findBook);
    }
}
