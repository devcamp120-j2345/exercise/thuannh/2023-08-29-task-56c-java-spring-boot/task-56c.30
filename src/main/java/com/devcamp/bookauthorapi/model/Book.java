package com.devcamp.bookauthorapi.model;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class Book {
    private String name;
    private double price;
    private int qty = 0;
    private ArrayList<Author> authors = new ArrayList<>();

    public Book() {
    }

    public Book(String name, double price, ArrayList<Author> authors) {
        this.name = name;
        this.price = price;
        this.authors = authors;
    }

    public Book(String name, double price, int qty) {
        this.name = name;
        this.price = price;
        this.qty = qty;
    }

    public Book(String name, double price, int qty, ArrayList<Author> authors) {
        this.name = name;
        this.price = price;
        this.qty = qty;
        this.authors = authors;
    }

    public String getAuthorNames() {
        return authors.stream().map(Author::getName).collect(Collectors.joining(", "));
    }

    @Override
    public String toString() {
        return "Book [name=" + name + ", price=" + price + ", qty=" + qty + ", authors=" + authors + "]";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public ArrayList<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(ArrayList<Author> authors) {
        this.authors = authors;
    }

}
