package com.devcamp.bookauthorapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.bookauthorapi.model.Author;
@Service
public class AuthorService {
    Author thuannh = new Author("Hữu Thuận", "thuannh@gmail.com", 'M');
    Author huypt = new Author("Tuấn Huy", "huypt@gmail.com", 'M');
    Author hoangdt = new Author("Tiến Hoàng", "hoangdt@gmail.com", 'M');
    Author ngaht = new Author("Thúy Nga", "ngaht@gmail.com", 'F');
    Author vynt = new Author("Thúy Vy", "vynt@gmail.com", 'F');
    Author yenvh = new Author("Hoàng Yến", "yenvh@gmail.com", 'F');

    public ArrayList<Author> getAuthorBook1() {
        ArrayList<Author> authorBook1 = new ArrayList<>();

        authorBook1.add(thuannh);
        authorBook1.add(yenvh);

        return authorBook1;
    }

    public ArrayList<Author> getAuthorBook2() {
        ArrayList<Author> authorBook2 = new ArrayList<>();

        authorBook2.add(huypt);
        authorBook2.add(vynt);

        return authorBook2;
    }

    public ArrayList<Author> getAuthorBook3() {
        ArrayList<Author> authorBook3 = new ArrayList<>();

        authorBook3.add(hoangdt);
        authorBook3.add(ngaht);

        return authorBook3;
    }

    public ArrayList<Author> getAllAuthors() {
        ArrayList<Author> allAuthor = new ArrayList<>();

        allAuthor.add(thuannh);
        allAuthor.add(huypt);
        allAuthor.add(hoangdt);
        allAuthor.add(ngaht);
        allAuthor.add(vynt);
        allAuthor.add(yenvh);

        return allAuthor;
    }

    public Author filterAuthorsByEmail(String authorEmail) {
        ArrayList<Author> authors = new ArrayList<>();

        authors.add(thuannh);
        authors.add(huypt);
        authors.add(hoangdt);
        authors.add(ngaht);
        authors.add(vynt);
        authors.add(yenvh);

        Author findAuthor = new Author();

        for (Author authorElement : authors) {
            if (authorElement.getEmail().equals(authorEmail)) {
                findAuthor = authorElement;
            }
        }
        return findAuthor;
    }

    public ArrayList<Author> filterAuthorsByGender(char authorGender) {
        ArrayList<Author> authors = new ArrayList<>();

        authors.add(thuannh);
        authors.add(huypt);
        authors.add(hoangdt);
        authors.add(ngaht);
        authors.add(vynt);
        authors.add(yenvh);

        ArrayList<Author> findAuthor = new ArrayList<>();

        for (Author authorElement : authors) {
            if (authorElement.getGender() == authorGender) {
                findAuthor.add(authorElement);
            }
        }

        return new ArrayList<>(findAuthor);
    }
}
