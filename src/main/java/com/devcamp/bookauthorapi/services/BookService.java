package com.devcamp.bookauthorapi.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.bookauthorapi.model.Book;

@Service
public class BookService extends AuthorService{
    @Autowired
    private AuthorService authorService;

    Book book1 = new Book("Book 1", 100000, 3);
    Book book2 = new Book("Book 2", 200000, 5);
    Book book3 = new Book("Book 3", 300000, 7);

    public ArrayList<Book> getAllBooks(){
        ArrayList<Book> allBook = new ArrayList<>();

        book1.setAuthors(authorService.getAuthorBook1());
        book2.setAuthors(authorService.getAuthorBook2());
        book3.setAuthors(authorService.getAuthorBook3());

        allBook.add(book1);
        allBook.add(book2);
        allBook.add(book3);

        return allBook;
    }

}
